﻿--
-- See how-to article on https://invantive.atlassian.net/wiki/spaces/SUP/pages/48791554/Time-travel+Your+ERP+data+using+Invantive+SQL and the
-- picture attached.
--
-- Replace hist@inmemorystorage by hist@sqlserver or another alias to maintain the history table in your database.
--
-- Set up data.
--
begin
  create or replace table t1@inmemorystorage
  as
  select cast(0 as int32) invoicenumber
  ,      'To delete invoice' description
  ,      to_date('20190101', 'YYYYMMDD') created
  ,      to_date('20190101', 'YYYYMMDD') modified
  union all
  select cast(1 as int32) invoicenumber
  ,      'First invoice' description
  ,      to_date('20190101', 'YYYYMMDD') created
  ,      to_date('20190101', 'YYYYMMDD') modified
  union all
  select cast(2 as int32) invoicenumber
  ,      'Second invoice' description
  ,      to_date('20190104', 'YYYYMMDD') created
  ,      to_date('20190104', 'YYYYMMDD') modified
  ;
  create or replace table t0@inmemorystorage
  as
  select *
  from   t1@InMemoryStorage
  where  false
  ;
  create or replace table t2@InMemoryStorage
  as
  --
  -- New situation contains all old invoices except the
  -- first invoice which was changed, and invoice 0 which
  -- was deleted, plus one new invoice.
  --
  select * except rowid$
  from   t1@inmemorystorage
  where  invoicenumber not in (0, 1)
  union all
  --
  -- Changed invoice 1.
  --
  select invoicenumber
  ,      'New first invoice; fixed typo.' description
  ,      created
  ,      to_date('20190212', 'YYYYMMDD') modified
  from   t1@InMemoryStorage
  where  invoicenumber = 1
  union all
  select cast(3 as int32) invoicenumber
  ,      'Third invoice' description
  ,      to_date('20190312', 'YYYYMMDD') created
  ,      to_date('20190312', 'YYYYMMDD') modified
  ;
end;

--
-- Initial setup of history table "hist".
--
begin
  if not exists (select /*+ ods(false) */ 1 from systemtables@datadictionary where name = 'HIST')
  then
    create or replace table hist@inmemorystorage
    as
    select cast(0 as int64) 
           h_id 
           label 'ID'
    ,      sys_context('USERENV', 'SESSIONID') 
           h_session_id 
           label 'Measurement Session ID'
    ,      sys_context('USERENV', 'CLIENT_MACHINE_NAME') 
           h_created_on 
           label 'Machine Created on'
    ,      sys_context('USERENV', 'USER_FULL_NAME') 
           h_created_by 
           label 'User Created by'
    ,      cast(sysdateutc as datetime) 
           h_date_measured 
           label 'Measured (UTC)'
    ,      cast(sysdateutc as datetime) 
           h_date_start 
           label 'Start Date (UTC)'
    ,      cast(sysdateutc as datetime) 
           h_date_end 
           label 'End Date (UTC)'
    ,      cast('X' as char) 
           h_event 
           label 'Event'
    ,      cast(true as boolean) 
           h_is_last_known 
           label 'Last Known State?'
    ,      t.* except rowid$
    from   t0@inmemorystorage t
    where  false
    ;
  end if;
end;

select *
from   T0@InMemoryStorage
order
by     invoicenumber

select *
from   T1@InMemoryStorage
order
by     invoicenumber

select *
from   T2@InMemoryStorage
order
by     invoicenumber

create or replace table OLD@InMemoryStorage
as
select *
from   t1@inmemorystorage

create or replace table NEW@InMemoryStorage
as
select *
from   t2@inmemorystorage

select cast(coalesce(h_id_max, 0) as int64) h_id_max_nn
from   ( select /*+ ods(false) */
                max(h_id) h_id_max
         from   hist@InMemoryStorage
       )

local define H_ID_LAST "${outcome:0,0}"

--
-- Take list of result sets (with partition column) and change them into time travel list.
--
insert
into   hist@inmemorystorage
select cast(to_number('${H_ID_LAST}') + row_number() as int64) h_id
,      sys_context('USERENV', 'SESSIONID') h_session_id
,      sys_context('USERENV', 'CLIENT_MACHINE_NAME') h_created_on
,      sys_context('USERENV', 'USER_FULL_NAME') h_created_by
,      sysdateutc h_date_measured
,      t.*
from   ( select created + (sysdateutc - sysdate) h_date_start
         ,      to_date('99991231', 'YYYYMMDD') h_date_end
         ,      'I' h_event
         ,      true h_is_last_known
         ,      new.* except rowid$
         from   new@InMemoryStorage new
         where  cast(new.invoicenumber as varchar2) /* Workaround bug InClause 28. */
                not in 
                ( select cast(invoicenumber as varchar2)
                  from   old@InMemoryStorage
                )
         union all
         select sysdateutc h_date_start
         ,      sysdateutc h_date_end
         ,      'D' h_event
         ,      false h_is_last_known
         ,      old.* except rowid$
         from   old@InMemoryStorage old
         where  cast(old.invoicenumber as varchar2)
                not in 
                ( select cast(invoicenumber as varchar2)
                  from   new@InMemoryStorage
                )
         union all 
         select new.modified + (sysdateutc - sysdate) h_date_start
         ,      to_date('99991231', 'YYYYMMDD') h_date_end
         ,      'U' h_event
         ,      true h_is_last_known
         ,      new.* except rowid$
         from   new@InMemoryStorage new
         join   old@InMemoryStorage old
         on     old.invoicenumber = new.invoicenumber
         --
         -- Rewrite this clause to a compare on all fields when no modified is available or not covering all scenarios.
         --
         and    old.modified != new.modified
       ) t

begin
  for r 
  in
  ( select /*+ ods(false) */ 
           h_id
    ,      h_date_start
    ,      invoicenumber
    from   hist@inmemorystorage
    where  h_id in ( select /*+ ods(false) */ max(h_id) from hist@InMemoryStorage group by invoicenumber )
  )
  loop
    --
    -- Update old records to be no longer last known.
    --
    update hist@inmemorystorage
    set    h_is_last_known = false
    where  h_is_last_known = true
    and    invoicenumber   = r.invoicenumber
    and    h_id            != r.h_id
    ;
    --
    -- Put an end date on records now no longer current.
    --
    update hist@inmemorystorage
    -- No 1 second offset as on Oracle with Invantive Producer, since Invantive SQL optimizes this better.
    set    h_date_end    = r.h_date_start
    where  h_date_end    = to_date('99991231', 'YYYYMMDD')
    and    invoicenumber = r.invoicenumber
    and    h_id          != r.h_id
    ;
  end loop;
end;

--
-- Memorize the old ultimate situation for use on the next run.
--
create or replace table old@PERSISTENTSTORAGE
as
select /*+ ods(false) */
       * 
from   old@inmemorystorage

select /*+ ods(false) */ * 
from   HIST@InMemoryStorage
order
by     h_id

--
-- Current records.
--
select /*+ ods(false) */
       *
from   HIST@InMemoryStorage
where  h_is_last_known = true
order
by     invoicenumber

--
-- Specific time.
--
select /*+ ods(false) */ 
       *
from   HIST@InMemoryStorage
where  to_date('20190401', 'YYYYMMDD') >= h_date_start 
and    to_date('20190401', 'YYYYMMDD') <  h_date_end
order
by     invoicenumber

--
-- Sample cartesian product. You can base a database view or
-- materialized view on this approach to ease creating sliders
-- for Power BI and alike.
-- Note that the data volume can be large and for a performant
-- environment with many rows you will need a database platform
-- that is able to forward the where clause with the data into
-- the view definition (Oracle, SQL Server recent versions).
--
select clr.day_date, h.* 
from   CALENDAR@DataDictionary clr
join   HIST@InMemoryStorage h
on     clr.day_date >= h.h_date_start
and    clr.day_date <  h.h_date_end
where  clr.day_date <= sysdateutc
