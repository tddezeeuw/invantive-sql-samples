local remark
local remark Invantive PSQL procedures to assist maintaining a large number
local remark of Invantive Data Replicator with each many partitions and tables.
local remark
local remark Feel free to use these procedures as a starting point for your own
local remark scripts. Support on these procedures is available only 
local remark based upon Time & Material.
local remark
local remark Requires Invantive SQL 17.31.
local remark
local remark (C) 2000-2019 Invantive Software BV, the Netherlands.
local remark

--
-- List of tables to be excluded.
--
-- The partition filter enables you to specify from what partitions the tables should be taken.
-- Possible values are:
-- * ACTIVE: all active partitions (company on Exact Online), except those listed in
--   xxdru_excluded_partitions.
-- * ACTIVESUB: the first active partition of every owning company 
--   (subscription on Exact Online), except those listed in
--   xxdru_excluded_partitions.
-- * SPECIFIC: one specific partition, listed in partition_code.
--
create or replace table xxdru_desired_tables@inmemorystorage
as
select 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' 
       full_qualified_name
,      'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' 
       partition_filter
,      'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' 
       partition_code
from   dual@DataDictionary
where  false

--
-- Partitions to be excluded from ACTIVE and ACTIVESUB specification in
-- xxdru_desired_tables.
--
create or replace table xxdru_excluded_partitions@inmemorystorage
as
select 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' 
       partition_code
from   dual@DataDictionary
where  false

create or replace table xxdru_sql_actions@inmemorystorage
as
select 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' action 
,      'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' tbe_full_qualified_name
,      'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
       sql_statement

--
-- Generic settings across all instances.
--
create or replace procedure xxdru_initialize
( p_agreement_code varchar2
)
is
begin
  set use-http-disk-cache false;
  set use-http-memory-cache false;
  set use-result-cache false;
  --
  -- For restarts.
  -- To be disabled.
  --
  --set http-disk-cache-max-age-sec 3600;
  set requests-parallel-max 4;
  -- Not yet covered by PSQL:
  -- alter persistent cache download feed license contract code p_agreement_code;
  delete xxdru_desired_tables@inmemorystorage;
  delete xxdru_excluded_partitions@inmemorystorage;
  delete xxdru_sql_actions@inmemorystorage;
end;

--
-- Select all partitions include in 'ACTIVE' set.
--
create or replace procedure xxdru_use_active
is
begin
  use
  select spn.code
  from   systempartitions@DataDictionary spn
  where  spn.data_container_alias = 'eol'
  and    spn.is_active = true
  and    spn.code not in (select partition_code from xxdru_excluded_partitions@inmemorystorage)
  order
  by     spn.code
  ;
end;

--
-- Defined a list of SQL statements in xxdru_sql_actions to make
-- the configuration of Data Replicator match the specifications given
-- in xxdru_desired_tables and xxdru_excluded_partitions.
--
-- The SQL statements can be found after execution in the table
-- xxdru_sql_actions. Statements with a value of 'MISSING' in the
-- column action will add data to Data Replicator. Statements with a
-- value of 'SUPERFLUOUS' will drop data from Data Replicator.
--
create or replace procedure xxdru_maintain_table_partitions
( p_execute_missing     boolean
, p_execute_superfluous boolean
)
is
  l_error_cnt pls_integer;
  l_error_txt varchar2;
begin
  create or replace table xxdru_temp_needed_actions@inmemorystorage
  as
  select 'xyz' tbe_full_qualified_name
  ,      'something' ptn_name
  from   empty@datadictionary
  ;
  create or replace table xxdru_temp_present_actions@inmemorystorage
  as
  select tbe.tbe_full_qualified_name
  ,      ptn.ptn_name
  from   dc_table_partitions@DataCache tpn
  join   dc_tables@datacache tbe
  on     tbe.tbe_id = tpn.tbe_id
  join   dc_partitions@datacache ptn
  on     ptn.ptn_id = tpn.ptn_id
  ;
  --
  -- Explode required replication set to list of tables and partitions.
  --
  for r_tbe in ( select * from xxdru_desired_tables@inmemorystorage )
  loop
    if r_tbe.partition_filter = 'ACTIVE'
    then
      insert into xxdru_temp_needed_actions@inmemorystorage
      ( tbe_full_qualified_name
      , ptn_name
      )
      select r_tbe.full_qualified_name
      ,      spn.code
      from   systempartitions@DataDictionary spn
      where  spn.data_container_alias = 'eol'
      and    spn.is_active = true
      and    spn.code not in (select partition_code from xxdru_excluded_partitions@inmemorystorage)
      order
      by      spn.code
      ;
    elsif r_tbe.partition_filter = 'ACTIVESUB'
    then
      insert into xxdru_temp_needed_actions@inmemorystorage
      ( tbe_full_qualified_name
      , ptn_name
      )
      select r_tbe.full_qualified_name
      ,      min(spn.code)
      from   systempartitions@DataDictionary spn
      where  spn.data_container_alias = 'eol'
      and    spn.is_active = true
      and    spn.code not in (select partition_code from xxdru_excluded_partitions@inmemorystorage)
      group
      by      spn.owner_company_code
      ;
    elsif r_tbe.partition_filter = 'SPECIFIC'
    then
      insert into xxdru_temp_needed_actions@inmemorystorage
      ( tbe_full_qualified_name
      , ptn_name
      )
      values
      ( r_tbe.full_qualified_name
      , r_tbe.partition_code
      );
    else
      raise_application_error(-20163, 'Unknown partition filter ' || r_tbe.partition_filter || '.');
    end if;
  end loop;
  --
  -- Translate into SQL actions.
  --
  create or replace table xxdru_temp_sql_foj@inmemorystorage
  as
  select coalesce(ist.ptn_name, soll.ptn_name) ptn_name
  ,      coalesce(ist.tbe_full_qualified_name, soll.tbe_full_qualified_name) tbe_full_qualified_name
  ,      case
         when ist.ptn_name is null 
         then 'MISSING'
         else 'SUPERFLUOUS'
         end
         action
  from   xxdru_temp_present_actions@inmemorystorage ist
  full
  outer
  join   xxdru_temp_needed_actions@InMemoryStorage soll
  on     ist.tbe_full_qualified_name = soll.tbe_full_qualified_name
  and    ist.ptn_name = soll.ptn_name
  where  ist.ptn_name is null or soll.ptn_name is null
  ;
  --
  -- Parallelizes new downloads.
  --
  create or replace table xxdru_sql_actions@inmemorystorage
  as
  select t.action 
  ,      t.tbe_full_qualified_name
  ,      'begin use ' || t.ptn_names || '; select count(*) from ' || t.tbe_full_qualified_name || '; end;'
         sql_statement
  from   ( select t.action
           ,      t.tbe_full_qualified_name
           ,      listagg(t.ptn_name) ptn_names 
           from   xxdru_temp_sql_foj@inmemorystorage t 
           where  t.action = 'MISSING' 
           group 
           by     t.action
           ,      t.tbe_full_qualified_name 
         ) t
  union all
  select t.action 
  ,      t.tbe_full_qualified_name
  ,      'alter persistent cache drop table ' || t.tbe_full_qualified_name || ' partition ''' || t.ptn_name || ''''
         sql_statement
  from   xxdru_temp_sql_foj@inmemorystorage t
  where  t.action = 'SUPERFLUOUS'
  union all
  --
  -- Drop partitions deemed superfluous
  --
  select 'SUPERFLUOUS'
  ,      ''
  ,      'alter persistent cache drop partition ' || partition_code
  from   xxdru_excluded_partitions@inmemorystorage epn
  join   dc_partitions@datacache ptn
  on     ptn.ptn_name = epn.partition_code
  ;
  --
  -- Execute SQL actions.
  --
  l_error_cnt := 0;
  l_error_txt := '';
  --
  for r in 
  ( select sql_statement 
    from   xxdru_sql_actions@inmemorystorage 
    where  sql_statement is not null 
    and    ( ( p_execute_missing = true and action = 'MISSING' )
             or
             ( p_execute_superfluous = true and action = 'SUPERFLUOUS' )
           )
  )
  loop
    begin
      execute immediate r.sql_statement;
    exception
      when others
      then
        l_error_cnt := l_error_cnt + 1;
        l_error_txt := l_error_txt || 'CAN NOT YET DETERMINE SQLM.';
    end;
  end loop;
  --
  if l_error_cnt > 0
  then
    raise_application_error(-20163, to_char(l_error_cnt) || ' errors occurred during maintenance of table partitions.');
  end if;
end;

--
-- Maintain trickle loading.
--
create or replace procedure xxdru_maintain_trickle_loading
( p_license_contract_code varchar2
)
is
begin
  for r_trickle in
  ( --
    -- Activate trickle loading for all tables for which
    -- trickle loading is available but not yet activated.
    --
    -- Selects all replicated tables that support webhooks (and
    -- therefore trickle loading). Restrict
    -- to those tables that have one or more table partitions
    -- (such as Exact Online companies) for which trickle loading
    -- is not yet activated.
    --
    -- Expected runtime on initial run in seconds: 5 times number of partitions (companies).
    -- Expected runtime after initial runs in seconds: 1.
    --
    select distinct 'alter persistent cache set table '
           || tbe.tbe_full_qualified_name
           || ' approach trickle'
           stmt
    from   dc_tables@datacache tbe    
    join   dc_table_partitions@DataCache tpn
    on     tpn.tbe_id = tbe.tbe_id
    and    tpn.tpt_seeding_approach_used_code = 'C' /* Complete. */
    join   SYSTEMTABLES@DataDictionary ste
    on     ste.full_qualified_name = tbe.tbe_full_qualified_name
    and    ste.webhook_topic is not null
    --
    -- Skip all XML tables, since none of them
    -- has webhooks anyway.
    --
    where  tbe.tbe_full_qualified_name like '%REST%'
  )
  loop
    execute immediate r_trickle.stmt || ' license contract code ''' || p_license_contract_code || '''';
  end loop;
end;