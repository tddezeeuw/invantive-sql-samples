local remark
local remark Sample script for Invantive SQL with Invantive Data Replicator
local remark connector to replicate thousands of Exact Online companies
local remark efficiently.
local remark
local remark Feel free to use these procedures as a starting point for your own
local remark scripts. Support on these procedures is available only 
local remark based upon Time & Material.
local remark
local remark Requires Invantive SQL 17.31.
local remark
local remark (C) 2000-2019 Invantive Software BV, the Netherlands.
local remark

local on error exit failure

local define AGREEMENT_CODE "L999999999"

local define REQUIRED_DATA_CONTAINER_ID "https://start.exactonline.nl/999999"

local define REQUIRED_USER_ID "MYUSER@DATACONTAINER.COM"

local define CLOUD_WATCH_PREFIX "SomeCustomerNamePrefix"

local remark *** END OF CONFIGURATION SECTION ***

alter session set billing id '${AGREEMENT_CODE}'

@@xxhosting.sql

@@xxdru.sql

begin
  xxhosting_initialize;
  xxhosting_check_user('${REQUIRED_DATA_CONTAINER_ID}', '${REQUIRED_USER_ID}');
  xxhosting_check_agreement('${AGREEMENT_CODE}');
end;

local on error continue

begin
  xxdru_initialize('${AGREEMENT_CODE}');
end;

insert into xxdru_excluded_partitions@inmemorystorage
( partition_code
)
values
( ( '1440405' )
)

alter persistent cache download feed license contract code '${AGREEMENT_CODE}'

begin
  xxdru_use_active;
end;

local show message "Refresh Data Replicator."

alter persistent cache purge droppable table partition versions

alter persistent cache refresh

insert into xxdru_desired_tables@inmemorystorage
( full_qualified_name
, partition_filter
, partition_code
)
values
( ( 'ExactOnlineREST.System.SystemDivisions', 'SPECIFIC', 'DEFAULT' )
, ( 'ExactOnlineREST.System.SystemDivisionClassifications', 'ACTIVESUB', null )
, ( 'ExactOnlineREST.Mailbox.Mailboxes', 'ACTIVESUB', null )
, ( 'ExactOnlineREST.Financial.Journals', 'ACTIVE', null )
, ( 'ExactOnlineREST.VAT.VATCodes', 'ACTIVE', null )
, ( 'ExactOnlineREST.Financial.GLAccounts', 'ACTIVE', null )
, ( 'ExactOnlineREST.HRM.Costcenters', 'ACTIVE', null )
, ( 'ExactOnlineREST.HRM.Costunits', 'ACTIVE', null )
, ( 'ExactOnlineREST.CRM.Accounts', 'ACTIVE', null )
, ( 'ExactOnlineXML.XML.FinPeriods', 'ACTIVE', null )
, ( 'ExactOnlineXML.XML.FinYears', 'ACTIVE', null )
, ( 'ExactOnlineXML.XML.BudgetLinesPerPeriod', 'ACTIVE', null )
, ( 'ExactOnlineXML.XML.BalanceLinesPerPeriod', 'ACTIVE', null )
, ( 'ExactOnlineREST.Financial.PayablesList', 'ACTIVE', null )
, ( 'ExactOnlineREST.Financial.ReceivablesList', 'ACTIVE', null )
, ( 'ExactOnlineREST.FinancialTransaction.TransactionLinesBulk', 'ACTIVE', null )
, ( 'ExactOnlineREST.Financial.GLAccountClassificationMappings', 'ACTIVE', null )
, ( 'ExactOnlineXML.XML.GLAccountClassificationRelations', 'ACTIVE', null )
)

begin
  xxdru_maintain_table_partitions(true, false);
end;

local show message "Maintain trickle loading."

begin
  xxdru_use_active;
  xxdru_maintain_trickle_loading('${AGREEMENT_CODE}');
end;

local show message "Register in CloudWatch."

begin
  xxhosting_register_progress('${CLOUD_WATCH_PREFIX}', 'xx', cast(to_number('${stat:errorcountcontinue}') as pls_integer));
end;

select name
,      value
from   systemdatacontainerproperties@datadictionary
where  provider_name = 'DataCache'
order
by     name

local export results as "c:\temp\${AGREEMENT_CODE}-data-cache-statistics.csv" format txt

local exit 0
