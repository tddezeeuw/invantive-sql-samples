﻿local remark Export Invantive Producer repository into graphml files for use by yEd.

local define INTERFACE_USER "xxx"

local define INTERFACE_PWD "xxx"

insert into bubs_pre_laden_r@ora(action_requested, action_parameter1, action_parameter2, action_parameter3, action_parameter4, action_parameter9) values ('LOGON FULL', 'xx', 'act', '${INTERFACE_USER}', 'xx', '${INTERFACE_PWD}')

local define APN_CODE "repos/eol/trunk"

local define ORIG_SYSTEM_GROUP_EXCLUDE "Exclude"

local define TARGET_ROOT_FOLDER "C:\ws\invantive-sql-samples\exact-online\Documentation"

local define TARGET_RELEASE_FOLDER "erd-2017"

local define TARGET_FILE_NAME_PREFIX "exact-online-erd-2017-"

create or replace table settings@inmemorystorage
as
select '<?xml version="1.0" encoding="utf-8"?>'
       || '<graphml xmlns:y="http://www.yworks.com/xml/graphml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://graphml.graphdrawing.org/xmlns/graphml">'
       || '<key for="node" id="d0" yfiles.type="nodegraphics"/>'
       || '<key attr.name="description" attr.type="string" for="node" id="d1"/>'
       || '<key for="edge" id="d2" yfiles.type="edgegraphics"/>'
       || '<key attr.name="description" attr.type="string" for="edge" id="d3"/>'
       || '<key for="graphml" id="d4" yfiles.type="resources"/>'
       || '<graph edgedefault="directed" id="G" parse.edges="2" parse.nodes="3" parse.order="free">'
       header
,      '</graph>'
       || '</graphml>'
       footer
from   dual@datadictionary

create or replace table nodelayouts@inmemorystorage
as
select 'Y' tbe_partition_flag
,      'R' tbe_data_ind
,      false tbe_is_copy
,      '#a8c94c' node_fill_color
,      '#8aaa17' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'S' tbe_data_ind
,      false tbe_is_copy
,      '#59a9df' node_fill_color
,      '#1d71b8' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'T' tbe_data_ind
,      false tbe_is_copy
,      '#f4242e' node_fill_color
,      '#bf1818' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'Y' tbe_partition_flag
,      'S' tbe_data_ind
,      false tbe_is_copy
,      '#c41d83' node_fill_color
,      '#9b0d68' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'Y' tbe_partition_flag
,      'T' tbe_data_ind
,      false tbe_is_copy
,      '#dc7600' node_fill_color
,      '#d15400' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'Y' tbe_partition_flag
,      'O' tbe_data_ind
,      false tbe_is_copy
,      '#999999' node_fill_color
,      '#666666' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'O' tbe_data_ind
,      false tbe_is_copy
,      '#ffbc00' node_fill_color
,      '#fca800' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'R' tbe_data_ind
,      false tbe_is_copy
,      '#6a3c8f' node_fill_color
,      '#461277' node_border_color
,      '#ffffff' node_text_color
,      '#c0c0c07e' node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'Y' tbe_partition_flag
,      'R' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#e2edc4' */ node_fill_color
,      '#444444' /* '#8aaa17' */ node_border_color
,      '#000000' /* '#8aaa17' */ node_text_color
,      '#cccccc' /* '#a8c94c' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'S' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#bfddf2' */ node_fill_color
,      '#444444' /* '#1d71b8' */ node_border_color
,      '#000000' /* '#1d71b8' */ node_text_color
,      '#cccccc' /* '#59a9df' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'T' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#fbb6b9' */ node_fill_color
,      '#444444' /* '#bf1818' */ node_border_color
,      '#000000' /* '#bf1818' */ node_text_color
,      '#cccccc' /* '#f4242e' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'Y' tbe_partition_flag
,      'S' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#f4bcdf' */ node_fill_color
,      '#444444' /* '#9b0d68' */ node_border_color
,      '#000000' /* '#9b0d68' */ node_text_color
,      '#cccccc' /* 'xxx' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'Y' tbe_partition_flag
,      'T' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#ffdbb2' */ node_fill_color
,      '#444444' /* '#d15400' */ node_border_color
,      '#000000' /* '#d15400' */ node_text_color
,      '#cccccc' /* '#dc7600' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'Y' tbe_partition_flag
,      'O' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#d8d8d8' */ node_fill_color
,      '#444444' /* '#666666' */ node_border_color
,      '#000000' /* '#666666' */ node_text_color
,      '#cccccc' /* '#999999' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'O' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#ffeab2' */ node_fill_color
,      '#444444' /* '#fca800' */ node_border_color
,      '#000000' /* '#fca800' */ node_text_color
,      '#cccccc' /* '#ffbc00' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary
union all
select 'N' tbe_partition_flag
,      'R' tbe_data_ind
,      true tbe_is_copy
,      '#eeeeee' /* '#dac9e7' */ node_fill_color
,      '#444444' /* '#461277' */ node_border_color
,      '#000000' /* '#461277' */ node_text_color
,      '#cccccc' /* '#6a3c8f' */ node_drop_shadow_color
,      45 node_height
,      200 node_width
,      15 node_font_size
from   dual@datadictionary

create or replace table tbebase@inmemorystorage
as
--
-- List of tables to be included.
--
select tbe.tbe_id
,      tbe.tbe_data_ind
,      tbe.tbe_partition_flag
,      tbe.tbe_orig_system_group
,      tbe.tbe_name
,      tbe.tbe_code
,      tbe.tbe_definition
,      tbe.ldy_label
from   itgen_tables_v@ora tbe
where  tbe.apn_code = '${APN_CODE}'
--and    tbe.tbe_id in ( select tbe_from_id from itgen_ref_constraints_v@ora union select tbe_to_id from itgen_ref_constraints_v@ora )
and    tbe.tbe_grant_select_flag = 'Y'
and    tbe.tbe_orig_system_group not in ( '${ORIG_SYSTEM_GROUP_EXCLUDE}' )

create or replace table rctbase@inmemorystorage
as
--
-- List of referential constraints to be included.
--
select rct.rct_id
,      rct.tbe_from_id
,      rct.tbe_to_id
,      rct.tbe_from_code
,      rct.tbe_to_code
,      rct.tbe_from_name
,      rct.tbe_to_name
,      rct.tcn_from_name
,      rct.tcn_to_name
,      tbefrom.tbe_orig_system_group tbe_from_orig_system_group
,      tbeto.tbe_orig_system_group tbe_to_orig_system_group
from   itgen_ref_constraints_v@ora rct
join   tbebase@inmemorystorage tbefrom
on     tbefrom.tbe_code = rct.tbe_from_code
and    tbefrom.tbe_orig_system_group not in ( '${ORIG_SYSTEM_GROUP_EXCLUDE}' )
join   tbebase@inmemorystorage tbeto
on     tbeto.tbe_code = rct.tbe_to_code
and    tbeto.tbe_orig_system_group not in ( '${ORIG_SYSTEM_GROUP_EXCLUDE}' )
where  rct.apn_from_code = '${APN_CODE}'

create or replace table tbecategories@inmemorystorage
as
select distinct
       tbe_orig_system_group
,      lower('${TARGET_FILE_NAME_PREFIX}' || tbe_orig_system_group || '.graphml') category_file_name
,      lower(tbe_orig_system_group) category_code
from   tbebase@inmemorystorage

create or replace table tbe_keys@inmemorystorage
as
select distinct
       tbe_to_name tbe_name
,      tbe_to_code tbe_code
,      tcn_to_name tcn_name
from   rctbase@inmemorystorage

create or replace table tbe_with_copies@inmemorystorage
as
--
-- Add copy of tables from other sub-systems when there is a referential constraint
-- between sub-systems.
--
select tcy.category_file_name
,      tcy.category_code
,      tbe.tbe_name tbe_name_old
,      tbe.tbe_name
       || chr(13)
       || tbe.tbe_code
       || case
          when key.tcn_name is not null
          then ', {key}' || key.tcn_name
          end
       tbe_name_new
,      tbe.tbe_code tbe_code_old
,      tbe.tbe_code
       tbe_code_new
,      tbe.tbe_partition_flag
,      tbe.tbe_data_ind
,      tbe.tbe_definition
,      tbe.ldy_label
,      tbe.tbe_orig_system_group
,      false
       tbe_is_copy
from   tbecategories@inmemorystorage tcy
join   tbebase@inmemorystorage tbe
on     tbe.tbe_orig_system_group = tcy.tbe_orig_system_group
left
outer
join   tbe_keys@inmemorystorage key
on     key.tbe_code = tbe.tbe_code
union all
select distinct 
       tcy.category_file_name
,      tcy.category_code
,      tbe.tbe_name tbe_name_old
,      tbe.tbe_name
       || chr(13)
       || case
          when tbe.tbe_orig_system_group != 'CodeValues'
          then tbe.tbe_orig_system_group || ': '
          end
       || tbe.tbe_code
       || case
          when key.tcn_name is not null
          then ', {key}' || key.tcn_name
          end
       tbe_name_new
,      tbe.tbe_code tbe_code_old
,      tbe.tbe_code
       || case
          when tbe.tbe_orig_system_group != 'CodeValues'
          then lower(tbe.tbe_orig_system_group)
          end
       tbe_code_new
,      tbe.tbe_partition_flag
,      tbe.tbe_data_ind
,      tbe.tbe_definition
,      tbe.ldy_label
,      tbe.tbe_orig_system_group
,      true
       tbe_is_copy
from   tbecategories@inmemorystorage tcy
join   rctbase@inmemorystorage rct
on     rct.tbe_from_orig_system_group = tcy.tbe_orig_system_group
and    rct.tbe_to_orig_system_group != tcy.tbe_orig_system_group
join   tbebase@inmemorystorage tbe
on     tbe.tbe_id = rct.tbe_to_id
left
outer
join   tbe_keys@inmemorystorage key
on     key.tbe_code = tbe.tbe_code

create or replace table rctfull@inmemorystorage
as
select tbefrom.category_file_name category_file_name
,      tbefrom.category_code category_code
,      rct.rct_id rct_id
,      tbefrom.tbe_code_new tbe_from_code_new
,      tbeto.tbe_code_new tbe_to_code_new
,      tbefrom.tbe_name_old tbe_from_name_old
,      tbeto.tbe_name_old tbe_to_name_old
,      rct.tcn_from_name
from   rctbase@inmemorystorage rct
join   tbe_with_copies@inmemorystorage tbefrom
on     tbefrom.tbe_code_old = rct.tbe_from_code
join   tbe_with_copies@inmemorystorage tbeto
on     tbeto.tbe_code_old = rct.tbe_to_code
where  ( ( tbefrom.tbe_is_copy = false and tbeto.tbe_is_copy = false)
         or
         ( tbefrom.tbe_is_copy = true and tbeto.tbe_is_copy = false)
         or
         ( tbefrom.tbe_is_copy = false and tbeto.tbe_is_copy = true)
       )
and    tbefrom.category_file_name = tbeto.category_file_name

create or replace table tberelcount@inmemorystorage
as
--
-- Per diagram and table the count of in- and outgoing relationships.
--
select coalesce(relcntout.category_file_name, relcntin.category_file_name) category_file_name
,      coalesce(relcntout.category_code, relcntin.category_code) category_code
,      coalesce(relcntout.tbe_code_new, relcntin.tbe_code_new) tbe_code_new
,      coalesce(relcntout.cnt, 0) cnt_out
,      coalesce(relcntin.cnt, 0) cnt_in
,      case
       when coalesce(relcntout.cnt, 0) > coalesce(relcntin.cnt, 0)
       then coalesce(relcntout.cnt, 0)
       else coalesce(relcntin.cnt, 0)
       end
       cnt_dir_max
from   ( select tbe_from_code_new tbe_code_new
         ,      tbe_from_name_old tbe_name_old
         ,      category_file_name
         ,      category_code
         ,      'OUT'
         ,      count(*) cnt
         from   rctfull@inmemorystorage tbe
         group
         by     tbe_from_code_new
         ,      tbe_from_name_old
         ,      category_file_name
         ,      category_code
       ) relcntout
full
outer
join   ( select tbe_to_code_new tbe_code_new
         ,      tbe_to_name_old tbe_name_old
         ,      category_file_name
         ,      category_code
         ,      'IN'
         ,      count(*) cnt
         from   rctfull@inmemorystorage tbe
         group
         by     tbe_to_code_new
         ,      tbe_to_name_old
         ,      category_file_name
         ,      category_code
       ) relcntin
on     relcntout.tbe_code_new       = relcntin.tbe_code_new
and    relcntout.category_file_name = relcntin.category_file_name

create or replace table nodes@inmemorystorage
as
select tbe.category_file_name
,      tbe.category_code
,      tbe.tbe_code_new
,      '<node id="t' || tbe.tbe_code_new || '">'
       || '<data key="d0">'
       || '<y:ShapeNode>'
       || '<y:Geometry height="'
       || case
          when trt.cnt_dir_max > 3
          then ceil(trt.cnt_dir_max * 15 / 45) * 45 + 10 /* Add space for rounded corners. */
          else coalesce(lyt.node_height, 60)
          end
       || '" width="'
       || coalesce(lyt.node_width, 60)
       || '" x="0" y="0"/>'
       || '<y:Fill color="' 
       || coalesce(lyt.node_fill_color, '#000000')
       || '" transparent="false"/>'
       || '<y:BorderStyle color="'
       || coalesce(lyt.node_border_color, '#000000')
       || '" type="line" width="1.0"/>'
       || '<y:NodeLabel alignment="center" autoSizePolicy="node_width"'
       || ' fontFamily="Gill Sans MT"'
       || ' fontSize="'
       || coalesce(lyt.node_font_size, 10)
       || '" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false"'
       || ' height="25" modelName="internal" modelPosition="c" textColor="'
       || coalesce(lyt.node_text_color, '#ffffff')
       || '" visible="true" width="100" x="0.0" y="0.0">'
       || replace(xmlencode(tbe.tbe_name_new), '{key}', '#') -- Key symbol &#1f511; not yet available broadly.
       || '</y:NodeLabel>'
       || '<y:DropShadow color="'
       || coalesce(lyt.node_drop_shadow_color, '#000000')
       || '" offsetX="2" offsetY="2" />'
       || '<y:Shape type="roundrectangle"/>'
       || '</y:ShapeNode>'
       || '</data>'
       || '<data key="d1">Description: '
       || xmlencode(tbe.tbe_definition)
       || chr(13)
       || 'Data Category: '
       || xmlencode(tbe.ldy_label)
       || chr(13)
       || 'Group: '
       || xmlencode(tbe.tbe_orig_system_group)
       || chr(13)
       || 'Partition: '
       || xmlencode(tbe.tbe_partition_flag)
       || '</data>'
       || '</node>'
       nodexml
from   tbe_with_copies@inmemorystorage tbe
left
outer
join   tberelcount@inmemorystorage trt
on     trt.category_file_name = tbe.category_file_name
and    trt.tbe_code_new       = tbe.tbe_code_new
left
outer
join   nodelayouts@inmemorystorage lyt
on     lyt.tbe_partition_flag = tbe.tbe_partition_flag
and    lyt.tbe_data_ind       = tbe.tbe_data_ind
and    lyt.tbe_is_copy        = tbe.tbe_is_copy

create or replace table relations@inmemorystorage
as
select rct.category_file_name
,      rct.category_code
,      '<edge id="e' 
       || rct.rct_id
       || '" source="t' 
       || rct.tbe_from_code_new
       || '" target="t'
       || rct.tbe_to_code_new
       || '">'
       || '<data key="d2">'
       || '<y:PolyLineEdge>'
       || '<y:Path sx="0.0" sy="0.0" tx="0.0" ty="0.0"/>'
       || '<y:LineStyle color="#000000" type="line" width="1.0"/>'
       || '<y:Arrows source="none" target="standard"/>'
       || '<y:EdgeLabel alignment="center" backgroundColor="#DAE7F7" distance="2.0"'
       || ' fontFamily="Gill Sans MT" lineColor="#5BABE0" fontSize="9" fontStyle="plain"'
       || ' height="25" modelName="centered" modelPosition="center"'
       || ' preferredPlacement="center" ratio="0.5" textColor="#000000" visible="true" width="100" x="0" y="0">'
       || xmlencode(rct.tcn_from_name)
       || '</y:EdgeLabel>'
       || '<y:BendStyle smoothed="true"/>'
       || '</y:PolyLineEdge>'
       || '</data>'
       || '</edge>'
       relationxml
from   rctfull@inmemorystorage rct

select stg.header 
       || nde.nodesxml
       || rct.relationsxml
       || stg.footer
       xml
,      case
       when nde.category_code in ('activities', 'accountancy', 'accounts', 'assets', 'budget', 'cashflow', 'documents', 'financial', 'invantive', 'mailbox', 'monitoring', 'system', 'transaction', 'users', 'vat', 'webhooks')
       then 'financial'
       else 'industry'
       end
       || '\'
       || nde.category_file_name
from   settings@inmemorystorage stg
join   ( select category_file_name, category_code, listagg(nde.nodexml, '') nodesxml from nodes@inmemorystorage nde group by category_file_name, category_code ) nde
join   ( select category_file_name, category_code, listagg(rct.relationxml, '') relationsxml from relations@inmemorystorage rct group by category_file_name, category_code ) rct
on     rct.category_file_name = nde.category_file_name
where  nde.category_code not in ( 'exclude', 'codevalues', 'xml' , 'monitoring')

local export documents in xml to "${TARGET_ROOT_FOLDER}\${TARGET_RELEASE_FOLDER}" filename column category_file_name
