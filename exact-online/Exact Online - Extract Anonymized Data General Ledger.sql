﻿local remark Retrieve a sample of balance posts from Exact Online across a number of divisions.

local define startyear "2015"

local define outpath "c:\temp"

local remark Determine which companies have some activity.

use select code from systemdivisions

create or replace table divisionswithactivity@inmemorystorage 
as 
select distinct 
       Division
from   ReportingBalance 
where  ReportingPeriod = 1 
and    ReportingYear = year(add_months(sysdate, -6)) - 1
and    AmountCredit != 0
and    BalanceType = 'W'

use select division from divisionswithactivity@inmemorystorage 

create or replace table systemdivisionsanon@inmemorystorage
as
select code
,      anonymize(code) code_anonymized
,      description
,      anonymize(description) description_anonymized
,      Currency
from   systemdivisions

select *
from   systemdivisionsanon@inmemorystorage
order
by     code_anonymized

local export results as "${outpath}\exact-gldump-map-divisions.csv" format csv

select code_anonymized
,      description_anonymized
,      Currency
from   systemdivisionsanon@inmemorystorage
order
by     code_anonymized

local export results as "${outpath}\exact-gldump-anon-divisions.csv" format csv

create or replace table finperiodsanon@inmemorystorage
as
select division
,      anonymize(division) division_anonymized
,      FinYear
,      FinPeriod
,      StartDate
,      EndDate
from   FinancialPeriods
where  FinYear >= ${startyear}

select *
from   finperiodsanon@inmemorystorage
order
by     division_anonymized
,      FinYear
,      FinPeriod

local export results as "${outpath}\exact-gldump-map-financial-periods.csv" format csv

select division_anonymized
,      FinYear
,      FinPeriod
,      StartDate
,      EndDate
from   finperiodsanon@inmemorystorage
order
by     division_anonymized
,      FinYear
,      FinPeriod

local export results as "${outpath}\exact-gldump-anon-financial-periods.csv" format csv

create or replace table glaccountsanon@inmemorystorage
as
select division
,      anonymize(division) division_anonymized
,      code
,      type
,      description
,      anonymize(description) 
       description_anonymized
,      balanceside
,      balancetype
from   exactonlinerest..glaccounts

select *
from   glaccountsanon@inmemorystorage
order
by     division_anonymized
,      code

local export results as "${outpath}\exact-gldump-map-general-ledger-accounts.csv" format csv

select division_anonymized
,      code
,      type
,      description_anonymized
,      balanceside
,      balancetype
from   glaccountsanon@inmemorystorage
order
by     division_anonymized
,      code

local export results as "${outpath}\exact-gldump-anon-general-ledger-accounts.csv" format csv

create or replace table glaccountclassificationsanon@inmemorystorage
as
select division
,      anonymize(division) division_anonymized
,      GLAccountCode
,      GLSchemeCode
,      ClassificationCode
,      ClassificationDescription
from   GLAccountClassificationMappings
where  GLSchemeCode = 'RGS versie 1.1'

select *
from   glaccountclassificationsanon@inmemorystorage
order
by     division_anonymized
,      GLSchemeCode
,      GLAccountCode
,      ClassificationCode

local export results as "${outpath}\exact-gldump-map-general-ledger-account-classifications.csv" format csv

select division_anonymized
,      GLAccountCode
,      GLSchemeCode
,      ClassificationCode
,      ClassificationDescription
from   glaccountclassificationsanon@inmemorystorage
order
by     division_anonymized
,      GLSchemeCode
,      GLAccountCode
,      ClassificationCode

local export results as "${outpath}\exact-gldump-anon-general-ledger-account-classifications.csv" format csv

create or replace table balancelinesperperiodanon@inmemorystorage
as
select division_code
,      anonymize(division_code) division_code_anonymized
,      periods_year_reportingyear_attr
,      reportingperiod_attr
,      periods_year_years_balance_code_attr
,      debit
,      credit
from   balancelinesperperiod 

select *
from   balancelinesperperiodanon@inmemorystorage
order 
by     division_code_anonymized
,      periods_year_reportingyear_attr
,      reportingperiod_attr
,      periods_year_years_balance_code_attr

local export results as "${outpath}\exact-gldump-map-balance-lines-per-period.csv" format csv

select division_code_anonymized
,      periods_year_reportingyear_attr
,      reportingperiod_attr
,      periods_year_years_balance_code_attr
,      debit
,      credit
from   balancelinesperperiodanon@inmemorystorage
order 
by     division_code_anonymized
,      periods_year_reportingyear_attr
,      reportingperiod_attr
,      periods_year_years_balance_code_attr

local export results as "${outpath}\exact-gldump-anon-balance-lines-per-period.csv" format csv
