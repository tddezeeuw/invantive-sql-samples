local remark
local remark Extract all Exact Online financial transactions into an Excel sheet.
local remark A separate Excel sheet is created per cost center.
local remark
local remark Requires Invantive SQL.
local remark

local remark
local remark Specify folder into which to place the composed output files.
local remark

local define OUTPUT_FOLDER "c:\temp\out"

local remark SQL Grammar: https://documentation.invantive.com/2017R2/invantive-query-tool/webhelp/invantive-sql-grammar.html
local remark Exact Online ER Diagram: https://documentation.invantive.com/2017R2/exact-online/Exact%20Online%20Data%20Model%20Poster%20A0.pdf
local remark Exact Online data model: https://documentation.invantive.com/2017R2/exact-online-data-model/webhelp/

local define FINANCIAL_YEAR "2018"

local define FINANCIAL_PERIOD "8"

create or replace table regels@inmemorystorage
as
select '${OUTPUT_FOLDER}' || '\\' || normalize('overview-' || tle.financialyear || '-' || tle.financialperiod || '-' || tle.CostCenter || '-' || tle.CostCenterDescription || '.xlsx', 200, false) filename
,      case
       when tle.GLAccountBalanceSide = 'D'
       then 'Expenses' 
       when tle.GLAccountBalanceSide = 'C' 
       then 'Income'
       else '?'
       end
       type
       label 'Type'
,      tle.CostCenter
,      tle.CostCenterDescription
,      tle.FinancialYear 
,      tle.FinancialPeriod
,      tle.CostUnit
,      tle.CostUnitDescription
,      tle.Date
,      tle.Description
,      tle.EntryNumber
,      case
       when tle.GLAccountBalanceSide = 'C' 
       then -1
       else +1
       end
       * tle.AmountDC 
       AmountDC
       label 'Amount (EUR)'
,      coalesce(rlt.Amount, plt.Amount) amount_open label 'Amount Open'
,      coalesce(rlt.DueDate, plt.DueDate) duedate label 'Due Date'
,      ( case
         when tle.GLAccountBalanceSide = 'C' 
         then -1
         else +1
         end
         * tle.AmountDC 
         - coalesce(rlt.Amount, plt.Amount, 0)
       )
       * case
         when tle.GLAccountBalanceSide = 'C' 
         then +1
         else -1
         end
       saldo_bank
       label 'Amount Bank'
,      tle.AccountName
,      tle.GLAccountCode
,      tle.GLAccountDescription
,      tle.JournalDescription
from   transactionlines tle
left
outer
join   receivableslist rlt
on     rlt.JournalCode     = tle.JournalCode
and    rlt.InvoiceNumber   = tle.InvoiceNumber
left
outer
join   payableslist plt
on     plt.JournalCode     = tle.JournalCode
and    plt.InvoiceNumber   = tle.InvoiceNumber
where  tle.financialyear   = ${FINANCIAL_YEAR}
and    tle.financialperiod = ${FINANCIAL_PERIOD}
and    tle.CostCenter is not null
order
by     filename
,      tle.FinancialYear
,      tle.FinancialPeriod

select filename
,      '001' sorting_order
,      type
,      CostCenter
,      CostCenterDescription
,      FinancialYear 
,      FinancialPeriod
,      CostUnit
,      CostUnitDescription
,      Date
,      Description
,      EntryNumber
,      AmountDC
,      amount_open
,      duedate
,      AccountName
,      GLAccountCode
,      GLAccountDescription
,      JournalDescription
,      saldo_bank
from   regels@inmemorystorage
union all
select filename
,      '002' sorting_order
,      'Total' type
,      null CostCenter
,      null CostCenterDescription
,      null FinancialYear 
,      null FinancialPeriod
,      null CostUnit
,      null CostUnitDescription
,      null Date
,      null Description
,      null EntryNumber
,      null AmountDC
,      null amount_open
,      null duedate
,      null AccountName
,      null GLAccountCode
,      null GLAccountDescription
,      null JournalDescription
,      sum(saldo_bank)
from   regels@inmemorystorage
group
by     filename
order
by     filename
,      sorting_order
,      Date

local remark Create one Excel file per cost center.

local export results using filename column filename format xlsx split on filename columns soort,CostCenter,CostCenterDescription,FinancialYear ,FinancialPeriod,CostUnit,CostUnitDescription,Date,Description,EntryNumber,AmountDC,amount_open,duedate,AccountName,GLAccountCode,GLAccountDescription,JournalDescription,saldo_bank include headers