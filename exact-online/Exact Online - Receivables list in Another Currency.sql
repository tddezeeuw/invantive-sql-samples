﻿local remark List of open invoices and the preferred currency of the customer

select rle.DueDate
,      rle.InvoiceNumber
,      rle.InvoiceDate
,      rle.accountcode
,      rle.accountname
,      rle.Description
,      rle.EntryNumber
,      rle.Amount
,      rle.AmountInTransit
,      act.SalesCurrency
,      ere.rate
,      ere.startdate
,      rle.Amount / ere.rate AmountInSalesCurrency
,      rle.AmountInTransit / ere.rate AmountInTransitInSalesCurrency
,      act.City
,      act.email
,      act.phone
,      act.language
from   receivableslist rle
join   exactonlinerest..accounts act
on     act.id = rle.accountid
left
outer
join   exactonlinerest..ExchangeRates ere
on     act.salescurrency != 'EUR'
and    ere.sourcecurrency = act.salescurrency
and    ere.startdate = trunc(sysdate)
order
by     rle.duedate desc
