create or replace table bml@inmemorystorage
as
select itm.ItemGroupCode
       || '.' || itm.code
       || '.' || ivn.versionnumber
       || ' - ' || itm.description 
       || ' ' || ivn.description
       itm_label
       label 'SKU Artikel'
,      itm.ItemGroupCode
       || '.' || itm.code
       || '.' || ivn.versionnumber
       || '.' || bml.LineNumber
       || ' - ' || itm.description
       || ' ' || ivn.description
       || ' regel ' || bml.LineNumber
       bml_label
       label 'Part Label'
,      itm.ItemGroupCode
,      itm.ItemGroupDescription
,      itm.code
,      itm.description
,      itm.Class_01
,      itm.Class_02
,      itm.Class_04
,      itm.Class_05
,      itm.IsSalesItem
,      itm.IsStockItem
,      itm.Unit
,      itm.UnitDescription
,      ivn.versionnumber
,      ivn.status
,      ivn.statusdescription
,      ivn.type
,      ivn.typedescription
,      bml.CostBatch
,      bml.Description
,      bml.LineNumber
,      bml.PartItemCode label 'Part Item Code'
,      bml.PartItemDescription label 'Part Item Description'
,      bml.PartItemCostPriceStandard label 'Part Item Cost Price (std)'
,      bml.Quantity
from   BillOfMaterialMaterials bml
left
outer
join   exactonlinerest..itemversions ivn
on     ivn.division = bml.division
and    ivn.id       = bml.itemversion
left
outer
join   exactonlinerest..items itm
on     itm.division = ivn.division
and    itm.id       = ivn.item
order
by     itm.ItemGroupCode
,      itm.code
,      ivn.versionnumber
,      bml.LineNumber

local export results as "c:\temp\dump-bill-of-material.xlsx" format xlsx