﻿local remark Dumps contents of ALL sales invoice lines for import.

select null Contract
,      act.code
,      sie.InvoiceDate
,      null StartDatePurchaseOrder
,      null EndDatePurchaseOrder
,      sil.AmountDC
,      sil.VATAmountDC
,      sil.VATCode
,      sil.description
,      sie.YourRef
,      sil.notes
,      sil.costcenter
,      sil.costunit
,      sil.project
,      null Department
from   exactonlinerest..salesinvoices sie
join   exactonlinerest..salesinvoicelines sil
on     sil.invoiceid = sie.invoiceid
join   exactonlinerest..accounts act
on     act.id = sie.invoiceto
order
by     sie.InvoiceDate

local export results as "c:\temp\sales-invoice-lines.csv" format csv

