﻿local remark Load journals, financial periods, GL account classifications and GL accounts into hundreds of divisions in one go.

local remark Please use XML export on your source division to generate the XML files.

local define SRC_FOLDER "c:\users\folder\Downloads"

insert into UploadXMLTopics
( topic
, payload
, division_code
, fragment_payload_flag
, fragment_max_size_characters
, orig_system_group
)
select 'GLTransactions'
,      rft.file_contents
,      sdn.code
,      true fragment_payload_flag
,      500000 fragment_max_size_characters 
,      'E' orig_system_group
from   systemdivisions sdn
join   read_file_text('${SRC_FOLDER}\GLTransactions_1.xml')@os rft
where  sdn.description like 'Leeg%'
union all
select 'Journals'
,      rft.file_contents
,      sdn.code
,      true fragment_payload_flag
,      100000 fragment_max_size_characters 
,      'A' orig_system_group
from   systemdivisions sdn
join   read_file_text('${SRC_FOLDER}\Journals_1.xml')@os rft
where  sdn.description like 'Leeg%'
union all
select 'FinYears'
,      rft.file_contents
,      sdn.code
,      true fragment_payload_flag
,      100000 fragment_max_size_characters 
,      'B' orig_system_group
from   systemdivisions sdn
join   read_file_text('${SRC_FOLDER}\FinYears_1.xml')@os rft
where  sdn.description like 'Leeg%'
union all
select 'GLAccountClassifications'
,      rft.file_contents
,      sdn.code
,      true fragment_payload_flag
,      100000 fragment_max_size_characters 
,      'C' orig_system_group
from   systemdivisions sdn
join   read_file_text('${SRC_FOLDER}\GLAccountClassifications_1.xml')@os rft
where  sdn.description like 'Leeg%'
union all
select 'GLAccounts'
,      rft.file_contents
,      sdn.code
,      true fragment_payload_flag
,      100000 fragment_max_size_characters 
,      'D' orig_system_group
from   systemdivisions sdn
join   read_file_text('${SRC_FOLDER}\GLAccounts_1.xml')@os rft
where  sdn.description like 'Leeg%'
order
by     orig_system_group
,      code