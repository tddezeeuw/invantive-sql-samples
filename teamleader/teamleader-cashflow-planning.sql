create or replace table projects@inmemorystorage
as
select pjt1.* prefix with 'pjt_'
,      pjt1.project_nr || ' - ' || pjt1.title pjt_label
-- TODO: Retrieve custom field 'betaling project'. Not yet supported.
-- ADD HERE CUSTOM FIELD betaling project
-- select * from custom_fields('PROJECT')
from   projects pjt
join   project(pjt.id) pjt1
where  pjt.phase = 'active'

create or replace table milestones@inmemorystorage
as
select pjt.pjt_id
,      pjt.pjt_title
,      pjt.pjt_project_nr
,      mse1.* prefix with 'mse_'
,      mse1.title mse_label
from   projects@inmemorystorage pjt
join   milestones_by_project(pjt.pjt_id) mse1
on     mse1.closed = false

create or replace table projectrestantbudgets@inmemorystorage
as
select pjt.pjt_id
,      pjt.pjt_budget_indication - coalesce(mse.mse_budget, 0) pjt_restant_budget
,      'End of project' mse_label
,      null mse_due_date
from   projects@inmemorystorage pjt
left
outer
join   ( select pjt_id, sum(mse_budget) mse_budget from milestones@inmemorystorage group by pjt_id ) mse
on     mse.pjt_id = pjt.pjt_id

create or replace table tasks@inmemorystorage
as
select mse.pjt_id
,      mse.pjt_title
,      mse.pjt_project_nr
,      mse.mse_id
,      mse.mse_title
,      tak1.* prefix with 'tak_'
,      substr(replace(replace(trim(tak1.description), chr(13), '.'), chr(10), '.'), 1, 60) tak_label
from   milestones@inmemorystorage mse
join   tasks_by_milestone(mse.mse_id) tak1
on     tak1.done = false

create or replace table cashflowlines@inmemorystorage
as
select pjt.pjt_label
,      mse.mse_label
,      'Milestone' cf_type
,      year(mse.mse_due_date) mse_due_date_year
,      month(mse.mse_due_date) mse_due_date_month
,      mse.mse_budget
from   projects@inmemorystorage pjt
join   milestones@inmemorystorage mse
on     mse.pjt_id = pjt.pjt_id
and    mse.mse_budget != 0

create or replace table cashflowlinestoplan@inmemorystorage
as
select pjt.pjt_label
,      mse.mse_label
,      'End-of-project' cf_type
,      year(mse.mse_due_date) mse_due_date_year
,      month(mse.mse_due_date) mse_due_date_month
,      mse.pjt_restant_budget
from   projects@inmemorystorage pjt
join   projectrestantbudgets@inmemorystorage mse
on     mse.pjt_id = pjt.pjt_id
and    mse.pjt_restant_budget != 0

create or replace table cashflowlinestoppers@inmemorystorage
as
select pjt.pjt_label
,      mse.mse_label
,      'Milestone' cf_type
,      year(mse.mse_due_date) mse_due_date_year
,      month(mse.mse_due_date) mse_due_date_month
,      mse.mse_budget
,      tak.tak_due_date
,      tak.tak_task_type
,      tak.tak_owner_name
,      tak.tak_label
from   projects@inmemorystorage pjt
join   milestones@inmemorystorage mse
on     mse.pjt_id = pjt.pjt_id
and    mse.mse_budget != 0
left
outer
join   tasks@inmemorystorage tak
on     tak.mse_id = mse.mse_id
and    tak.tak_task_type in ('Concept contract', 'Afronden document')
