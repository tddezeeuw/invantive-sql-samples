local remark
local remark Dump all data of one XAF 3.2 file to a spreadsheet per table.
local remark
local remark Runs on any (free) Open Data or (paid) All Cloud license of Invantive SQL-based products such
local remark as Invantive Query Tool, Invantive Control or Invantive Business.
local remark
local remark *** USAGE STEPS ***
local remark
local remark Execute the following steps:
local remark
local remark * Create a folder c:\temp\xaf-input.
local remark * Copy the XAF 3.2 file to c:\temp\xaf-input.
local remark * Create a folder c:\temp\xaf-output.
local remark * Start Invantive Business, Query Tool, Control or alike.
local remark * Choose the XAF 3.2 database connector.
local remark * For directories enter: c:\temp\xaf-input.
local remark * Press Connect.
local remark * For Invantive Business: go to Development menu and then Query Tool.
local remark * For Invantive Control: go to ribbon Modeler and then Query Tool.
local remark * Open this file through File menu and then Open.
local remark * Press F5.
local remark * Inspect output in the folder c:\temp\xaf-output.
local remark

local define OUT_PATH "c:\temp\xaf-output"

select companyident
from   companies

local define COMPANY_ID "${outcome:0,0}"

--
-- Dump statements generated using:
-- select 'select *' || chr(13) || 'from   ' || name || chr(13) || chr(13) || 'local export results as "${OUT_PATH}\${COMPANY_ID}-' || name || '.xlsx" format xlsx include headers' || chr(13) || chr(13)
-- from   systemtables@datadictionary
-- where  PROVIDER_SHORT_NAME = 'xaf'
-- order
-- by     name
--
select *
from   AccountTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-AccountTypeCodes.xlsx" format xlsx include headers


select *
from   BasicTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-BasicTypeCodes.xlsx" format xlsx include headers


select *
from   Companies

local export results as "${OUT_PATH}\${COMPANY_ID}-Companies.xlsx" format xlsx include headers


select *
from   CompanyPostalAddresses

local export results as "${OUT_PATH}\${COMPANY_ID}-CompanyPostalAddresses.xlsx" format xlsx include headers


select *
from   CompanyStreetAddresses

local export results as "${OUT_PATH}\${COMPANY_ID}-CompanyStreetAddresses.xlsx" format xlsx include headers


select *
from   CounterParties

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterParties.xlsx" format xlsx include headers


select *
from   CounterPartyBankAccounts

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterPartyBankAccounts.xlsx" format xlsx include headers


select *
from   CounterPartyHistories

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterPartyHistories.xlsx" format xlsx include headers


select *
from   CounterPartyHistoryBankAccounts

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterPartyHistoryBankAccounts.xlsx" format xlsx include headers


select *
from   CounterPartyHistoryPostalAddresses

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterPartyHistoryPostalAddresses.xlsx" format xlsx include headers


select *
from   CounterPartyHistoryStreetAddresses

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterPartyHistoryStreetAddresses.xlsx" format xlsx include headers


select *
from   CounterPartyPostalAddresses

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterPartyPostalAddresses.xlsx" format xlsx include headers


select *
from   CounterPartyStreetAddresses

local export results as "${OUT_PATH}\${COMPANY_ID}-CounterPartyStreetAddresses.xlsx" format xlsx include headers


select *
from   CustomerSupplierTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-CustomerSupplierTypeCodes.xlsx" format xlsx include headers


select *
from   GeneralLedgerAccounts

local export results as "${OUT_PATH}\${COMPANY_ID}-GeneralLedgerAccounts.xlsx" format xlsx include headers


select *
from   GeneralLedgerBasics

local export results as "${OUT_PATH}\${COMPANY_ID}-GeneralLedgerBasics.xlsx" format xlsx include headers


select *
from   GeneralLedgerTaxonomies

local export results as "${OUT_PATH}\${COMPANY_ID}-GeneralLedgerTaxonomies.xlsx" format xlsx include headers


select *
from   GeneralLedgerTaxonomyElements

local export results as "${OUT_PATH}\${COMPANY_ID}-GeneralLedgerTaxonomyElements.xlsx" format xlsx include headers


select *
from   Headers

local export results as "${OUT_PATH}\${COMPANY_ID}-Headers.xlsx" format xlsx include headers


select *
from   InvoiceDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-InvoiceDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   InvoicePurchaseSalesTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-InvoicePurchaseSalesTypeCodes.xlsx" format xlsx include headers


select *
from   JournalTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-JournalTypeCodes.xlsx" format xlsx include headers


select *
from   OpeningBalanceLineDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-OpeningBalanceLineDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   OpeningBalanceLines

local export results as "${OUT_PATH}\${COMPANY_ID}-OpeningBalanceLines.xlsx" format xlsx include headers


select *
from   OpeningBalances

local export results as "${OUT_PATH}\${COMPANY_ID}-OpeningBalances.xlsx" format xlsx include headers


select *
from   Periods

local export results as "${OUT_PATH}\${COMPANY_ID}-Periods.xlsx" format xlsx include headers


select *
from   SubledgerLineAmmountDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerLineAmmountDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   SubledgerLineDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerLineDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   SubledgerLineInvoiceDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerLineInvoiceDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   SubledgerLineInvoicePurchaseSalesTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerLineInvoicePurchaseSalesTypeCodes.xlsx" format xlsx include headers


select *
from   SubledgerLineMutationTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerLineMutationTypeCodes.xlsx" format xlsx include headers


select *
from   SubledgerOpeningBalanceLines

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerOpeningBalanceLines.xlsx" format xlsx include headers


select *
from   SubledgerOpeningBalances

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerOpeningBalances.xlsx" format xlsx include headers


select *
from   SubledgerTransactionLines

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerTransactionLines.xlsx" format xlsx include headers


select *
from   SubledgerTransactions

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerTransactions.xlsx" format xlsx include headers


select *
from   SubledgerTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-SubledgerTypeCodes.xlsx" format xlsx include headers


select *
from   TransactionAmountDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-TransactionAmountDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   TransactionJournals

local export results as "${OUT_PATH}\${COMPANY_ID}-TransactionJournals.xlsx" format xlsx include headers


select *
from   TransactionLineAmountDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-TransactionLineAmountDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   TransactionLines

local export results as "${OUT_PATH}\${COMPANY_ID}-TransactionLines.xlsx" format xlsx include headers


select *
from   TransactionRegisters

local export results as "${OUT_PATH}\${COMPANY_ID}-TransactionRegisters.xlsx" format xlsx include headers


select *
from   Transactions

local export results as "${OUT_PATH}\${COMPANY_ID}-Transactions.xlsx" format xlsx include headers


select *
from   TrialBalancePerPeriod

local export results as "${OUT_PATH}\${COMPANY_ID}-TrialBalancePerPeriod.xlsx" format xlsx include headers


select *
from   VATAmountDebitCreditTypeCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-VATAmountDebitCreditTypeCodes.xlsx" format xlsx include headers


select *
from   VatCodes

local export results as "${OUT_PATH}\${COMPANY_ID}-VatCodes.xlsx" format xlsx include headers
